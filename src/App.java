public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer(1, "Duong", 25);
        Customer customer2 = new Customer(2, "Linh", 50);

        System.out.println("2 doi tuong customer");
        System.out.println(customer1);
        System.out.println(customer2);

        Account account1 = new Account(4, customer1);
        Account account2 = new Account(4, customer2, 2000);

        System.out.println("2 doi tuong account");
        System.out.println(account1);
        System.out.println(account2);

        // chuyển thêmm tiền vào 2 đôi tượng
        account1.deposit(100);
        account2.deposit(100);
        System.out.println("2 doi tuong dc cong tien");
        System.out.println(account1);
        System.out.println(account2);

        // chuyển rut tiền 2 đôi tượng
        account1.withdraw(1000);
        account2.withdraw(1000);
        System.out.println("2 doi tuong bi tru tien");
        System.out.println(account1);
        System.out.println(account2);
    }
}
