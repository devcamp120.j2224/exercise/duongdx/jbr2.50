public class Account {
    private int id;
    private Customer customer ;
    private double banance ;
    
    public Account(int id, Customer customer, double banance) {
        this.id = id;
        this.customer = customer;
        this.banance = banance;
    }

    public Account(int id, Customer customer) {
        this.id = id;
        this.customer = customer;
    }

    public int getId() {
        return id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public double getBanance() {
        return banance;
    }

    public void setBanance(double banance) {
        this.banance = banance;
    }

    @Override
    public String toString() {
        return "Account [id=" + id + ", banance=" + banance + ", customer=" + customer +"]";
    }

    public String getCustomerName(){
        return customer.getName() ;
    }

    public double deposit(double amount){
        return banance += amount ;
    }

    public double withdraw(double amount){
        if(this.banance >= amount){
            this.banance -= amount ;
        }else{
            System.out.println("amount withdraw exceeds the current balance!");
        }
        return this.banance;
    }
    
}
